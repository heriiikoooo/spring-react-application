package com.springreact.backend.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class ItemController {

    @Autowired
    ItemRepository itemRepository;

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping(path = "/api/items")
    public List<Item> getAllItems(){
        List<Item> items = new ArrayList<>();
        itemRepository.findAll().forEach(items :: add);
        return items;
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping(path = "/api/items/{id}")
    public Optional<?> getById(@PathVariable int id){

        return itemRepository.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping(path = "/api/items/edit/{id}")
    public Optional<?> editById(@PathVariable int id){

        return itemRepository.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @PostMapping(path = "/api/items")
    public Item addItem(@RequestBody Item item){
        itemRepository.save(item);
        return item;
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @DeleteMapping(path = "/api/items/{id}")
    public void deleteItem(@PathVariable int id){
            itemRepository.deleteById(id);
    }
}