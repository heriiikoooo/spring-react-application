package com.springreact.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;

import com.springreact.backend.flowable.FlowableService;

@SpringBootApplication()
public class BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}

@Bean
public CommandLineRunner init(final FlowableService flowableService) {

    return new CommandLineRunner() {
        public void run(String... strings) throws Exception {
            flowableService.createDemoUsers();
        }
    };
}

}