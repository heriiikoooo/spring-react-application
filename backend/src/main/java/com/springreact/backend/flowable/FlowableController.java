package com.springreact.backend.flowable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

// import com.springreact.backend.flowable.FlowableService;

import java.util.ArrayList;
import org.flowable.task.api.Task;
import java.util.List;
import org.springframework.http.MediaType;
// import org.flowable.engine.TaskService;
import com.springreact.backend.flowable.Person;

@RestController
public class FlowableController {

    @Autowired
    private FlowableService flowableService;

    @Autowired
    PersonRepository personRepository;

    @CrossOrigin(origins = "http://localhost:8081")
    @PostMapping(path="/process")
    public void startProcessInstance(@RequestBody StartProcessRepresentation startProcessRepresentation) {
        flowableService.startProcess(startProcessRepresentation.getAssignee());
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @GetMapping(path = "/processes")
    public List<Person> getAllPersons(){
        List<Person> items = new ArrayList<>();
        personRepository.findAll().forEach(items :: add);
        return items;
    }

    @CrossOrigin(origins = "http://localhost:8081")
    @RequestMapping(value="/tasks", method= RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public List<TaskRepresentation> getTasks(@RequestParam String assignee) {
        List<Task> tasks = flowableService.getTasks(assignee);
        List<TaskRepresentation> dtos = new ArrayList<TaskRepresentation>();
        for (Task task : tasks) {
            dtos.add(new TaskRepresentation(task.getId(), task.getName()));
        }
        return dtos;
    }

    static class TaskRepresentation {

        private String id;
        private String name;

        public TaskRepresentation(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }

    }

    static class StartProcessRepresentation {

        private String assignee;

        public String getAssignee() {
            return assignee;
        }

        public void setAssignee(String assignee) {
            this.assignee = assignee;
        }
    }

}