package com.springreact.backend.flowable;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
@Repository
public interface PersonRepository extends JpaRepository<Person, String> {

    Person findByUsername(String username);
}