import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
// import AppNavbar from './AppNavbar.js';
// import { Link } from 'react-router-dom';

class FlowableTask extends Component {

  constructor(props) {
    super(props);
    this.state = {items: [], isLoading: true};
    // this.remove = this.remove.bind(this);
  }

  componentDidMount() {
    this.setState({isLoading: true});

    fetch('flowable/task')
      .then(response => response.json())
      .then(data => this.setState({items: data, isLoading: false}));
  }


  render() {
    const {items, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    const itemList = items.map(item => {
    //   const address = `${group.address || ''} ${group.city || ''} ${group.stateOrProvince || ''}`;
    return <div class="crm-task-list-container">

    <mat-nav-list>

      <a 
         mat-list-item>

        <mat-icon matListIcon class="crm-task-list-icon">
          assignment
        </mat-icon>

        <p mat-line>
          {item.name}
        </p>

        <p mat-line>
          {item.description}
        </p>

      </a>

    </mat-nav-list>

  </div>
    });

    // return (
    //   <div>
    //     <AppNavbar/>
    //     <Container fluid>
    //       <div className="float-right">
    //         <Button color="success" tag={Link} to="/items/add">Add Item</Button>
    //       </div>
    //       <h3>My Items</h3>
    //       <Table className="mt-4">
    //         <thead>
    //         <tr>
    //           <th width="20%">ID</th>
    //           <th width="20%">Name</th>
    //           <th>Quantity</th>
    //           <th width="10%">Actions</th>
    //         </tr>
    //         </thead>
    //         <tbody>
    //         { itemList }
    //         </tbody>
    //       </Table>
    //     </Container>
    //   </div>
    // );
  }
}

export default FlowableTask;