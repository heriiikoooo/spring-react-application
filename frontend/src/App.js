import React, { Component } from 'react';
import './App.css';
import Home from './items/Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Items from './items/Items';
import ItemEdit from './items/ItemEdit';
import FlowableTask from './flowable/FlowableTask';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/' exact={true} component={Home}/>
          <Route path='/items' exact={true} component={Items}/>
          <Route path='/items/edit/:id' component={ItemEdit}/>
          <Route path='/items/add' component={ItemEdit}/>
          {/* <Route path ="/flowable/task" component={FlowableTask}/> */}
        </Switch>
      </Router>
    )
  }
}

export default App;