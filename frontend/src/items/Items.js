import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import AppNavbar from './AppNavbar.js';
import { Link } from 'react-router-dom';

class Items extends Component {

  constructor(props) {
    super(props);
    this.state = {items: [], isLoading: true};
    this.remove = this.remove.bind(this);
  }

  componentDidMount() {
    this.setState({isLoading: true});

    fetch('api/items')
      .then(response => response.json())
      .then(data => this.setState({items: data, isLoading: false}));
  }

  async remove(id) {
    await fetch(`/api/item/${id}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(() => {
      let updatedItems = [...this.state.items].filter(i => i.id !== id);
      this.setState({items: updatedItems});
    });
  }

  render() {
    const {items, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    const itemList = items.map(item => {
    //   const address = `${group.address || ''} ${group.city || ''} ${group.stateOrProvince || ''}`;
      return <tr key={item.id}>
        <td style={{whiteSpace: 'nowrap'}}>{item.id}</td>
        <td>{ item.name }</td>
        <td>{ item.quantity }</td>
        <td>
          <ButtonGroup>
            <Button size="sm" color="primary" tag={Link} to={"/items/edit/" + item.id}>Edit</Button>
            <Button size="sm" color="danger" onClick={() => this.remove(item.id)}>Delete</Button>
          </ButtonGroup>
        </td>
      </tr>
    });

    return (
      <div>
        <AppNavbar/>
        <Container fluid>
          <div className="float-right">
            <Button color="success" tag={Link} to="/items/add">Add Item</Button>
          </div>
          <h3>My Items</h3>
          <Table className="mt-4">
            <thead>
            <tr>
              <th width="20%">ID</th>
              <th width="20%">Name</th>
              <th>Quantity</th>
              <th width="10%">Actions</th>
            </tr>
            </thead>
            <tbody>
            { itemList }
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default Items;