import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import AppNavbar from './AppNavbar';

class ItemEdit extends Component {

  emptyItem = {
    id: '',
    name: '',
    quantity: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      item: this.emptyItem
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    if (this.props.match.url !== '/items/add') {
      const group = await (await fetch(`/api/items/edit/${this.props.match.params.id}`)).json();
      // console.log(group)
      this.setState({item: group});
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let item = {...this.state.item};
    item[name] = value;
    this.setState({item});
  }

  async handleSubmit(event) {
    event.preventDefault();
    const {item} = this.state;
    // console.log(item)

    await fetch('/api/items', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item),
    });
    this.props.history.push('/items');
  }

  render() {
    const {item} = this.state;
    const title = <h2>{item.id ? 'Edit Item' : 'Add Item'}</h2>;

    const submitButton = item.id ? 'Update' : 'Save'


    return <div>
      <AppNavbar/>
      <Container>
        {title}
        <Form onSubmit={this.handleSubmit}>
            <FormGroup>
                <Label for="id">ID</Label>
                <Input type="text" name="id" id="id" value={item.id || ''}
                    onChange={this.handleChange} autoComplete="id" readOnly = {item.id ? true: false}/>
            </FormGroup>
            <FormGroup>
                <Label for="name">Name</Label>
                <Input type="text" name="name" id="name" value={item.name || ''}
                    onChange={this.handleChange} autoComplete="name"/>
            </FormGroup>
            <FormGroup>
                <Label for="quantity">Quantity</Label>
                <Input type="text" name="quantity" id="quantity" value={item.quantity || ''}
                    onChange={this.handleChange} autoComplete="quantity"/>
            </FormGroup>
            <FormGroup>
                <Button color="primary" type="submit">{submitButton}</Button>{' '}
                <Button color="secondary" tag={Link} to="/items">Cancel</Button>
            </FormGroup>
        </Form>
      </Container>
    </div>
  }
}

export default withRouter(ItemEdit);